a. Name - Paul Downing
b. Date - 09/29/2019
c. Platform - Windows 7
d. Notes to compile - The image folder must be in src
e. Known Issues - There is no scroll bar for when images are too large
There is extra space in the scene when the images are smaller
f. Approach - I first tried to create the scene in Scene Builder, but I could not get images to display from the Controller.
I decided to abandon FXML and create the project entirely in JavaFX. I first had to get an image to load. After this, I had to get two images
to load. The images loaded but they were stacked on top of each other. I then put the images in an AnchorPane and set the 2nd image to anchor
below the first image. A few of the images overlapped, so I also had to set the first image to anchor to the top of the screen. The next step
was to get random images from a set to display. Once that was implemented correctly, I had to create a button which would reload the images
when pressed in order to allow for infinitely usablility without having to reload the program. Unfortunately, the button was not centered and
occasionally not visible due to some image pairs taking up the entire screen. I had wanted to implement a scroll bar, but after researching,
it seemed that I would have to overhaul the design of the program and put the AnchorPane inside of a ScrollPane and I decided I did not have
the time to do this. After abandoning the scroll bar idea, I then centered the generate button.