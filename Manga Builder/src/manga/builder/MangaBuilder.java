/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package manga.builder;

import javafx.application.Application;
//import javafx.fxml.FXMLLoader;
//import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import java.util.Random;
import javafx.scene.control.Button;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;



/**
 *
 * @author Paul
 */
public class MangaBuilder extends Application {
    
    private final String address[] = {"ababa.png", "disappointment.png", "doyourbest.png", "face.png", "humanexperimentation.png",
    "late.png", "nice.png", "noexperimentation.png", "police.png", "smug.png", "terrible.jpg", "wow.png"};
    
    //method to generate a random image address from given array
    private int generate(String[] images){
        Random rand = new Random();
        return rand.nextInt(images.length);
    }
    
    //reruns the program
    private void restart(Stage stage){
        run(stage);
    }
    
    //method to actually run the program
    public void run(Stage stage){
        //Create two random images and their imageview
        ImageView iv1 = new ImageView();
        ImageView iv2 = new ImageView();
        Image panel1 = new Image("/images/"+address[generate(address)]);
        Image panel2 = new Image("/images/"+address[generate(address)]);
        
        //Button creation
        Button button = new Button("Generate another!");
        
        //variables for difference in width
        Double offset = 0.0;
        Boolean panel1isBigger = false;
        Boolean panel2isBigger = false;
        
        //center the image if needed
        if(panel1.getWidth() > panel2.getWidth()) {
            panel1isBigger = true;
            offset = (panel1.getWidth() - panel2.getWidth())/2;
        }
        else if(panel1.getWidth() < panel2.getWidth()) {
            panel2isBigger = true;
            offset = (panel2.getWidth() - panel1.getWidth())/2;
        }
        
        //set image in imageview
        iv1.setImage(panel1);
        iv2.setImage(panel2);
        
        //create pane
        AnchorPane root = new AnchorPane();
        //add child to pane
        root.getChildren().add(iv1);
        //sometimes the image doesn't stay at the top of the screen
        root.setTopAnchor(iv1, 0.0);
        //set anchor at the bottom of the image
        root.setBottomAnchor(iv1, panel1.getHeight());
        //move image right if needed
        if(panel2isBigger)
            root.setLeftAnchor(iv1, offset);
        
        //add child to pane and set image at top of the previous image's bottom anchor
        root.getChildren().add(iv2);
        root.setTopAnchor(iv2, root.getBottomAnchor(iv1));
        //set another bottom anchor at the bottom of this image
        root.setBottomAnchor(iv2, root.getBottomAnchor(iv1)+panel2.getHeight());
        //move image right if needed
        if(panel1isBigger)
            root.setLeftAnchor(iv2, offset);
        
        //add button
        root.getChildren().add(button);
        //set button below 2nd panel
        root.setTopAnchor(button, root.getBottomAnchor(iv2));
        //center button
        if(panel2isBigger)
            root.setLeftAnchor(button, (panel2.getWidth()/2)-55);
        else
            root.setLeftAnchor(button, (panel1.getWidth()/2)-55);
        
        //action event on button press
        EventHandler<ActionEvent> event = new EventHandler<ActionEvent>() { 
            public void handle(ActionEvent e) 
            { 
                restart(stage); 
            } 
        }; 
        button.setOnAction(event);
        
        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.show();
    }
    
    //calls the method to run program
    @Override
    public void start(Stage stage) throws Exception {
        //Parent root = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
        
        stage.setTitle("Manga Builder");
        
        run(stage);
    }
    

    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
